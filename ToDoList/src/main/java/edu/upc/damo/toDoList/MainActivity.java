package edu.upc.damo.toDoList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends Activity {
    private Boolean b=true;
    private Button boto;
    private ListView listView;
    private ArrayList<String> dades =new ArrayList<String>();

    private ArrayAdapter<String> adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        carregaDades();
        inicialitza();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void carregaDades(){

        String [] dadesEstatiques = getResources().getStringArray(R.array.dadesEstatiques);

        Collections.addAll(dades, dadesEstatiques);
    }

    private void afegirDada()
    {
        if(b) {
            dades.add("W");        // Podem modificar la font de dades directament
            Toast.makeText(this,"Modificat el model", Toast.LENGTH_LONG).show();;
        }
        else {
            adaptador.add("Y");    // Podem modificar la font de dades a través de l'adapter
            Toast.makeText(this,"Modificat l'adapter", Toast.LENGTH_LONG).show();;
        }
        adaptador.notifyDataSetChanged();
        b = !b;
    }
    private void inicialitza(){

        listView = (ListView) findViewById(R.id.llista);
        adaptador = new ArrayAdapter<String >(this,android.R.layout.simple_list_item_1,dades);




        listView.setAdapter(adaptador);

        boto = (Button) findViewById(R.id.button);
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afegirDada();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Esborra l'objecte polsat
                // dades.remove((int)parent.getAdapter().getItemId(position));   // Recuperem l'adapter del listview
                dades.remove((int)adaptador.getItemId(position));                // Useml'adapter que mantenim com a atribut

                // Esborra un objecte que coincideixi amb el polsat
                // dades.remove(adaptador.getItem(position));   // Modifiquem les dades
                //  adaptador.remove(adaptador.getItem(position));   // Modifiquem a través de l'adapter
                //  ((ArrayAdapter<String>)parent.getAdapter()).remove((String)parent.getAdapter().getItem(position));
                // Modifiquem a través de l'adapter que recuperemel ListView

                return true;
            }
        });

    }
}
